FROM ubuntu:jammy

LABEL keep=true
ENV ANDROID_SDK_ROOT /usr/lib/android-sdk
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/platform-tools
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV CHROMIUM_BIN /usr/bin/chromium-browser
ENV DOCKER_TLS_CERTDIR=/certs
ENV YARN_GLOBALS=/usr/local/share/.config/yarn/global
ENV NODE_PATH=${YARN_GLOBALS}/node_modules

RUN set -eux && \
    apt update && \
    apt -y install curl wget gpg git software-properties-common ca-certificates apt-transport-https && \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
    curl -sL https://deb.nodesource.com/setup_18.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    curl -sSfL https://apt.octopus.com/public.key |  apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    echo "deb https://apt.octopus.com/ stable main" | tee /etc/apt/sources.list.d/octopus.com.list && \
    apt update && \
    apt -y install iptables openssl pigz xz-utils \
    jq locales locales-all \
    yarn nodejs build-essential \
    octopuscli && \
    mkdir /certs /certs/client && \
    chmod 1777 /certs /certs/client && \
    npm install -g @angular/cli && \
    wget -O- https://apt.corretto.aws/corretto.key | apt-key add - && \
    add-apt-repository --yes 'deb https://apt.corretto.aws stable main' && \
    apt update && \
    mkdir -p /usr/share/man/man1 && \
    apt -y install java-17-amazon-corretto-jdk && \
    apt -y install java-21-amazon-corretto-jdk && \
    apt -y install java-11-amazon-corretto-jdk android-sdk && \
    update-alternatives --set java /usr/lib/jvm/java-11-amazon-corretto/bin/java && \
    rm -rf /var/lib/apt/lists/* && \
    cd /opt && wget -q https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip -O android-sdk-tools.zip && \
    mkdir -p ${ANDROID_HOME}/cmdline-tools && \
    unzip -q android-sdk-tools.zip -d ${ANDROID_HOME}/cmdline-tools && mv ${ANDROID_HOME}/cmdline-tools/cmdline-tools ${ANDROID_HOME}/cmdline-tools/tools && \
    rm android-sdk-tools.zip && \
    cd - && \
    yes | sdkmanager  --licenses && \
    touch /root/.android/repositories.cfg && \
    sdkmanager "tools" "platform-tools" && \
    yes | sdkmanager --update --channel=3 && \
    yes | sdkmanager \
    "platforms;android-32" \
    "platforms;android-31" \
    "build-tools;32.0.0" \
    "build-tools;31.0.0" \
    "build-tools;30.0.3" \
    "extras;android;m2repository" \
    "extras;google;m2repository" && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm awscliv2.zip && \
    apt-get update && \
    apt-get install -y \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg \
	--no-install-recommends && \
	curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - && \
	echo "deb https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list && \
	apt-get update && \
    apt-get install -y \
	google-chrome-stable \
	fontconfig \
	fonts-ipafont-gothic \
	fonts-wqy-zenhei \
	fonts-thai-tlwg \
	fonts-kacst \
	fonts-symbola \
	fonts-noto \
	fonts-freefont-ttf \
	--no-install-recommends && \
    curl -sL https://sentry.io/get-cli/ | bash && \
    # Set prefix for yarn config \
    yarn config set prefix /usr/local && \
    # Install playwright dependencies
    apt-get install -y \
    libx264-163 \
    libgles2 \
    libflite1 \
    libmanette-0.2-0 \
    libhyphen0 \
    libsecret-1-0 \
    libenchant-2-2 \
    libwebpdemux2 \
    libopenjp2-7 \
    libwebkit2gtk-4.0-37 \
    libgstreamer-plugins-bad1.0-0 \
    libharfbuzz0b \
    woff2 \
    libxslt1.1 \
    libopus0 \
    libevent-2.1-7 \
    libvpx7 \
    libsoup2.4-1 \
    libxtst6 \
    libdbus-glib-1-2 \
    --no-install-recommends && \
    # Install playwright dependency & browsers \
    yarn global add @playwright/test@1.49.0 && \
    npx playwright@1.49.0 install --with-deps && \
	rm -rf /var/lib/apt/lists/*

COPY --from=docker/compose-bin:v2.17.0 /docker-compose /usr/bin/docker-compose
COPY --from=docker:20.10.23-dind /usr/local/bin/ /usr/local/bin/

VOLUME /var/lib/docker
