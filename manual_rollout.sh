#!/bin/bash
set -eu

command_exists() {
  command -v "$1" >/dev/null 2>&1
}

if ! command_exists pv || ! command_exists gzip; then
  echo "Error: This script requires pv and gzip. Please install them."
  exit 1
fi

if [ $# -eq 0 ]
  then
    echo "Please add the target tag version as the first param: ./manual_rollout.sh 2.1.2"
    exit 1
fi

VERSION="$1"
TARGET_TAG="liftric/compose-debian-jdk-yarn-angular:$VERSION"

echo "going to create $TARGET_TAG"
sleep 2

echo "building..."
echo ""
docker build . -t "$TARGET_TAG"

echo ""
echo "save, then pipe to target..."
docker save "$TARGET_TAG" | gzip | pv | ssh liftric@192.168.1.59 'gunzip | /usr/bin/docker load'
